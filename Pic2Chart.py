#!python3
# -*- coding:utf-8 -*-

from PyQt4 import QtCore
from PyQt4 import QtGui
import xlsxwriter
import os,sys
import subprocess, platform
#import icons


class MainFrm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainFrm, self).__init__(parent)

        self.count = 0
        self.x_min_click = 0
        self.y_min_click = 0
        self.x_max_click = 0
        self.y_max_click = 0
        self.previewpos = QtCore.QPointF(0, 0)

        #界面
        self.Gui_x_y_min_max()
        self.Gui_pic_loader()
        self.Gui_data_grid()
        self.Gui_bottom_control()

        #主要布局
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.addLayout(self.bounary)
        self.mainLayout.addLayout(self.piclayout)
        self.mainLayout.addLayout(self.datagridlayout)
        self.mainLayout.addLayout(self.bottomcontrollayout)
        self.mainLayout.setStretch(1,1)
        self.centwid=QtGui.QLabel()
        self.centwid.setLayout(self.mainLayout)
        self.setCentralWidget(self.centwid)
        self.resize(640,650)
        self.setWindowTitle(u"图片坐标转换辅助工具")

    def Gui_x_y_min_max(self):
        self.x_min=QtGui.QLineEdit('0')
        self.y_min = QtGui.QLineEdit('0')
        self.x_max = QtGui.QLineEdit('100')
        self.y_max = QtGui.QLineEdit('100')
        self.bounary = QtGui.QHBoxLayout()
        self.bounary.addWidget(QtGui.QLabel('X最小值:'))
        self.bounary.addWidget(self.x_min)
        self.bounary.addWidget(QtGui.QLabel('X最大值:'))
        self.bounary.addWidget(self.x_max)
        self.bounary.addWidget(QtGui.QLabel('Y最小值:'))
        self.bounary.addWidget(self.y_min)
        self.bounary.addWidget(QtGui.QLabel('Y最大值:'))
        self.bounary.addWidget(self.y_max)

    def Gui_pic_loader(self):
        spacers = QtGui.QSpacerItem(10, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.myscene = myQGraphicsScene(self)

        self.Btn_picselect = QtGui.QPushButton('选择图片')
        self.Btn_picselect.clicked.connect(self.LoadPicture)

        self.piclselectlayout = QtGui.QHBoxLayout()
        self.piclselectlayout.addWidget(QtGui.QLabel('图片：      '))
        self.piclselectlayout.addItem(spacers)
        self.piclselectlayout.addWidget(self.Btn_picselect)

        self.piclayout = QtGui.QVBoxLayout()
        self.mypic = customGraphicsView()
        self.mypic.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.mypic.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.mypic.setScene(self.myscene)
        self.connect(self.mypic, QtCore.SIGNAL('resize()'),self.resizeimg)

        self.piclayout.addLayout(self.piclselectlayout)
        self.piclayout.addWidget(self.mypic, 1)


    def Gui_data_grid(self):
        self.datagridlayout = QtGui.QVBoxLayout()
        self.datagrid = QtGui.QTableWidget()
        self.datagridlayout.addWidget(QtGui.QLabel('数据：      '))
        self.datagridlayout.addWidget(self.datagrid)
        self.populatedatagrid()

        #右键菜单
        # 必须将ContextMenuPolicy设置为Qt.CustomContextMenu
        # 否则无法使用customContextMenuRequested信号
        self.datagrid.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.datagrid.customContextMenuRequested.connect(self.rightMenuShow)

    def Gui_bottom_control(self):
        self.bottomcontrollayout = QtGui.QHBoxLayout()
        self.chart_zhu = QtGui.QRadioButton('柱状图')
        self.chart_zhexian = QtGui.QRadioButton('折线图')
        self.chart_zhu.setChecked(True)
        self.chart_xydata = QtGui.QCheckBox('xy数据')
        spacers = QtGui.QSpacerItem(10,1,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Fixed)
        self.cpgriddata = QtGui.QPushButton('复制表格数据')
        self.cpgriddata.clicked.connect(self.tablecopy)
        self.export2xls = QtGui.QPushButton('导出至Excel并生成图表')
        self.export2xls.clicked.connect(self.exp2xls)
        self.bottomcontrollayout.addWidget(QtGui.QLabel('生成图表类型：'))
        self.bottomcontrollayout.addWidget(self.chart_zhu)
        self.bottomcontrollayout.addWidget(self.chart_zhexian)
        self.bottomcontrollayout.addWidget(self.chart_xydata)
        self.bottomcontrollayout.addItem(spacers)
        self.bottomcontrollayout.addWidget(self.cpgriddata)
        self.bottomcontrollayout.addWidget(self.export2xls)

        #右键菜单
        # 必须将ContextMenuPolicy设置为Qt.CustomContextMenu
        # 否则无法使用customContextMenuRequested信号
        self.datagrid.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.datagrid.customContextMenuRequested.connect(self.rightMenuShow)

    # 创建右键菜单
    def rightMenuShow(self, pos):
        rightMenu = QtGui.QMenu(self.datagrid)

        self.copydata = QtGui.QAction(QtGui.QIcon(":/image/icons/gnome-stock-text-indent.png"), u"(&C)复制数据", self)
        self.copydata.setShortcut('Ctrl+C')
        self.copydata.setStatusTip(u"复制数据")
        self.connect(self.copydata,QtCore.SIGNAL('triggered()'), self.tablecopy)

        rightMenu.addAction(self.copydata)

        action = rightMenu.exec_(self.datagrid.mapToGlobal(pos))

    def tablecopy(self):
        clip = ''
        row = self.datagrid.rowCount()
        col = self.datagrid.columnCount()
        for i in range(0,row):
            for j in range(0, col):
                if j ==col-1:
                    clip = clip + self.datagrid.item(i, j).text() + "\n"
                else:
                    clip = clip + self.datagrid.item(i,j).text() + "\t"

        clipboard = QtGui.QApplication.clipboard()
        clipboard.setText(clip)

    def exp2xls(self):
        workbook = xlsxwriter.Workbook('expchart.xlsx')
        worksheet = workbook.add_worksheet()
        bold = workbook.add_format({'bold': 1})

        # Add the worksheet data that the charts will refer to.
        headings = ['X', 'Y']
        data = []
        datax = []
        datay = []
        row = self.datagrid.rowCount()
        col = self.datagrid.columnCount()
        for i in range(0,col):
            for j in range(0, row):
                if i == 0:
                    datax.append(float(self.datagrid.item(j, i).text()))
                if i ==1:
                    datay.append(float(self.datagrid.item(j, i).text()))
        data.append(datax)
        data.append(datay)
        worksheet.write_row('A1', headings, bold)
        worksheet.write_column('A2', data[0])
        worksheet.write_column('B2', data[1])

        # Create a new column chart.
        if self.chart_zhu.isChecked():
            chart1 = workbook.add_chart({'type': 'column'})
        else:
            chart1 = workbook.add_chart({'type': 'scatter',
                             'subtype': 'straight_with_markers'})
        # Configure the first series.
        chart1.add_series({
            'name': '=Sheet1!$B$1',
            'categories': '=Sheet1!$A$2:$A$'+str(row+1),
            'values': '=Sheet1!$B$2:$B$'+str(row+1),
    })
        # Add a chart title and some axis labels.
        # if self.chart_zhu.isChecked():
        #     chart1.set_title({'name': '柱状图'})
        # else:
        #     chart1.set_title({'name': '折线图'})

        #设置坐标轴
        chart1.set_x_axis({
            #'name': 'X',
            #'date_axis': True,
            'min': float(self.x_min.text()),
            'max': float(self.x_max.text()),
            #'num_format': 'dd/mm/yyyy',
        })
        chart1.set_y_axis({
            #'name': 'Y',
            #'date_axis': True,
            'min': float(self.y_min.text()),
            'max': float(self.y_max.text()),
            #'num_format': 'dd/mm/yyyy',
        })

        # Set an Excel chart style.
        chart1.set_style(11)

        # Insert the chart into the worksheet (with an offset).
        worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10})

        workbook.close()

        if platform.system() == 'Windows':
            subprocess.Popen('expchart.xlsx', shell=True)

        if platform.system() == 'Linux':
            subprocess.Popen('libreoffice --calc expchart.xlsx', shell=True)

    def LoadPicture(self):
        self.previewpos = QtCore.QPointF(0, 0)
        for i in range(0,self.datagrid.rowCount()):
            self.datagrid.removeRow(0)
        self.count = 0
        self.x_min_click = 0
        self.y_min_click = 0
        self.x_max_click = 0
        self.y_max_click = 0
        fname = QtGui.QFileDialog.getOpenFileName(None, '导入图片', '', '图片文件(*.png *.jpg *.bmp *.gif)')
        if fname== None:
            pass
        else:
            self.pixmap = QtGui.QPixmap()
            self.pixmap.load(fname)
            myScaledPixmap = self.pixmap.scaled(self.mypic.size())

            item = QtGui.QGraphicsPixmapItem(myScaledPixmap)
            self.myscene.addItem(item)
            self.mypic.setScene(self.myscene)

    def resizeimg(self):
        try:
            myScaledPixmap = self.pixmap.scaled(self.mypic.size())
            item = QtGui.QGraphicsPixmapItem(myScaledPixmap)
            self.myscene.addItem(item)
            self.mypic.setScene(self.myscene)
        except:
            pass

    def populatedatagrid(self, selectedShip=None):
        selected = None
        self.datagrid.setSortingEnabled(False)
        headers = [u"X", u"Y"]
        self.datagrid.setColumnCount(2)
        self.datagrid.setHorizontalHeaderLabels(headers)

class customGraphicsView(QtGui.QGraphicsView):

    def __init__(self, parent=None):
        QtGui.QGraphicsView.__init__(self, parent)
        self.setMouseTracking(True)

    def resizeEvent(self, evt=None):
        self.emit(QtCore.SIGNAL("resize()"))

    # def mouseMoveEvent(self, event):
    #     print('mouseMoveEvent: pos {}'.format(event.pos()))

    # def mouseReleaseEvent(self, event):
    #     print('mouseReleaseEvent: pos {}'.format(event.pos()))

    # def mousePressEvent(self, event):
    #     print('mousePressEvent: pos {}'.format(event.pos()))
    #     point = QtGui.QGraphicsEllipseItem(event.pos().x(),event.pos().y(),5,5)
    #     self.scene().addItem(point)

class myQGraphicsScene(QtGui.QGraphicsScene):
    def __init__ (self, parent=None):
        super(myQGraphicsScene, self).__init__ (parent)

    def mousePressEvent(self, event):
        super(myQGraphicsScene, self).mousePressEvent(event)
        print('mousePressEvent: pos {}'.format(event.pos()))
        if event.pos().x() ==0 and event.pos().y() == 0:
            pass
        else:
            if self.parent().count==0 or self.parent().count==1 or self.parent().count==2:
                if self.parent().count == 0:
                    self.parent().x_min_click = event.pos().x()
                    self.parent().y_min_click = event.pos().y()
                    txt = QtGui.QGraphicsTextItem(' 坐标原点', None, self)
                    txt.setPos(event.pos().x(),event.pos().y())
                    txt.setDefaultTextColor(QtCore.Qt.red)
                if self.parent().count == 1:
                    self.parent().x_max_click = event.pos().x()
                    txt = QtGui.QGraphicsTextItem(' X轴最大值', None, self)
                    txt.setPos(event.pos().x(),event.pos().y())
                    txt.setDefaultTextColor(QtCore.Qt.red)
                if self.parent().count == 2:
                    self.parent().y_max_click = event.pos().y()
                    txt = QtGui.QGraphicsTextItem(' Y轴最大值', None, self)
                    txt.setPos(event.pos().x(),event.pos().y())
                    txt.setDefaultTextColor(QtCore.Qt.red)

                item = QtGui.QGraphicsEllipseItem(event.pos().x(),event.pos().y(),8,8)
                item.setBrush(QtCore.Qt.blue)
                item.setPen(QtCore.Qt.red)

                self.addItem(item)
                self.addItem(txt)

                self.parent().count+=1
            else:
                self.parent().count += 1
                #开始计算
                x_len_click = self.parent().x_max_click-self.parent().x_min_click
                y_len_click = self.parent().y_max_click - self.parent().y_min_click
                x_min = float(self.parent().x_min.text())
                y_min = float(self.parent().y_min.text())
                x_max = float(self.parent().x_max.text())
                y_max = float(self.parent().y_max.text())
                x_len = x_max-x_min
                y_len = y_max-y_min
                #推算出真实坐标
                x_real = x_min + ((event.pos().x()-self.parent().x_min_click)/x_len_click)*x_len
                y_real = y_min + ((event.pos().y() - self.parent().y_min_click) / y_len_click) * y_len
                item = QtGui.QGraphicsEllipseItem(event.pos().x(),event.pos().y(),5,5)
                item.setBrush(QtCore.Qt.red)
                item.setPen(QtCore.Qt.red)
                self.addItem(item)

                self.parent().datagrid.insertRow(self.parent().datagrid.rowCount())
                self.parent().datagrid.scrollToBottom()
                if self.parent().chart_xydata.isChecked():
                    self.parent().datagrid.setItem(self.parent().datagrid.rowCount() - 1, 0,
                                                   QtGui.QTableWidgetItem(str(x_real)))
                else:
                    self.parent().datagrid.setItem(self.parent().datagrid.rowCount() - 1, 0,
                                                   QtGui.QTableWidgetItem(
                                                       str(int(self.parent().x_min.text()) + self.parent().count - 3)))
                self.parent().datagrid.setItem(self.parent().datagrid.rowCount() - 1, 1,
                                                QtGui.QTableWidgetItem(str(y_real)))

                if self.parent().previewpos.x()==0 and self.parent().previewpos.y()==0 :
                    pass
                else:
                    lineitem = QtGui.QGraphicsLineItem(self.parent().previewpos.x() + 2.5,
                                                       self.parent().previewpos.y() + 2.5, event.pos().x() + 2.5,
                                                       event.pos().y() + 2.5)
                    lineitem.setPen(QtCore.Qt.red)
                    self.addItem(lineitem)
                self.parent().previewpos = event.pos()


def main():
    app = QtGui.QApplication(sys.argv)
    app.setOrganizationName("Gansu Research Institute for Water Conservancy")
    app.setOrganizationDomain("www.gsssky.com")
    app.setApplicationName("Image coordinate conversion tool")
    app.setWindowIcon(QtGui.QIcon(":/logo/icon/chart.png"))

    form = MainFrm()
    form.show()
    app.exec_()    
if __name__=="__main__":
    main()
